package nl.codecentric.rijks.activities.detail

import android.content.Intent
import androidx.test.core.app.ApplicationProvider
import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.gson.Gson
import nl.codecentric.rijks.ArtDetails
import nl.codecentric.rijks.HeaderImage
import nl.codecentric.rijks.R
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ItemDetailActivityAndroidTest {


    private fun getTestIntent(): Intent {
        return Intent(
            ApplicationProvider.getApplicationContext(),
            ItemDetailActivity::class.java
        ).apply {
            putExtra(
                ArtDetailFragment.ART_DETAILS_KEY, Gson().toJson(
                    ArtDetails(
                        objectNumber = "TEST_objectNumber",
                        title = "TEST_title",
                        longTitle = "TEST_longTitle",
                        headerImage = HeaderImage(
                            guid = "TEST_guid",
                            offsetPercentageX = 111,
                            offsetPercentageY = 222,
                            width = 333,
                            height = 444,
                            url = "TEST_url"
                        )
                    )
                )
            )
        }
    }

    @Test
    fun assertText() {
        val intent = getTestIntent()

        val scenario = launchActivity<ItemDetailActivity>(intent)
        scenario.recreate()

        onView(withId(R.id.item_detail_1)).check(matches(withText("TEST_longTitle")));
    }

    @Test
    fun assertFailText() {
        val intent = getTestIntent()

        val scenario = launchActivity<ItemDetailActivity>(intent)
        scenario.recreate()

        onView(withId(R.id.item_detail_1)).check(matches(withText("something else")));
    }
}