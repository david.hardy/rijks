package nl.codecentric.rijks.activities.master

import androidx.fragment.app.FragmentManager
import nl.codecentric.rijks.ArtDetails
import nl.codecentric.rijks.HeaderImage
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mockito
import org.mockito.Mockito.*


class RijksViewAdapterTest {

    private lateinit var mockFragmentManager: FragmentManager;
    private lateinit var rijksViewAdapterSpy: RijksViewAdapter;

    @Before
    fun setUp() {
        this.mockFragmentManager = mock(FragmentManager::class.java)
    }

    @Test
    fun returnsLoadingTypeWhenBindingForNullData() {
        rijksViewAdapterSpy =
            RijksViewAdapter(mockFragmentManager, false, 10) { _currentSize: Int, _pageSize: Int ->
            }
        assert(rijksViewAdapterSpy.getItemViewType(0) == RijksViewAdapter.VIEWTYPE_LOADING)
    }

    @Test
    fun returnsDataTypeWhenBindingForNullData() {
        rijksViewAdapterSpy =
            Mockito.spy( RijksViewAdapter(mockFragmentManager, false, 10) { _currentSize: Int, _pageSize: Int ->
            })
        doNothing().`when`(rijksViewAdapterSpy).notifyDataSetChanged();
        doNothing().`when`(rijksViewAdapterSpy).notifyItemRangeInserted(ArgumentMatchers.anyInt(), ArgumentMatchers.anyInt());

        val artObjects = listOf(
            ArtDetails(
                objectNumber = "TEST_objectNumber",
                title = "TEST_title",
                longTitle = "TEST_longTitle",
                headerImage = HeaderImage(
                    guid = "TEST_guid",
                    offsetPercentageX = 111,
                    offsetPercentageY = 222,
                    width = 333,
                    height = 444,
                    url = "TEST_url"
                )
            )
        )
        rijksViewAdapterSpy.addData(artObjects)
        verify(rijksViewAdapterSpy, times(1)).notifyDataSetChanged();

        rijksViewAdapterSpy.addData(artObjects)
        verify(rijksViewAdapterSpy, times(1)).notifyItemRangeInserted(1,1);
    }

    @Test
    fun callLoadmoreWhenBindingNearEnd() {
        var currentSize: Int? = null
        var pageSize: Int? = null

        rijksViewAdapterSpy =
            Mockito.spy( RijksViewAdapter(mockFragmentManager, false, 111) { _currentSize: Int, _pageSize: Int ->
        currentSize =_currentSize;
        pageSize =_pageSize;
            })

        val mockViewHolder = mock(RijksViewAdapter.BaseViewHolder::class.java)

        rijksViewAdapterSpy.onBindViewHolder(mockViewHolder, 0);

        assertThat(currentSize, `is`(equalTo(0)))
        assertThat(pageSize, `is`(equalTo(111)))
    }
}