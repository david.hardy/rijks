package nl.codecentric.rijks.backend

import android.util.Log
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RijksKeyInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val url = chain.request().url().newBuilder()
            .addQueryParameter("key", "0fiuZFh4")
            .build()
        Log.d("TAG", "intercept: url: " + url)
        val request = chain.request().newBuilder()
            .url(url)
            .build()
        return chain.proceed(request)
    }
}

class RijksRertofit {
    fun rijksService(): RijksService {
        return retrofit().create(RijksService::class.java)
    }

    private fun retrofit(): Retrofit {
        val client = OkHttpClient.Builder()
            .addInterceptor(RijksKeyInterceptor())
            .build()
        return Retrofit.Builder()
            .baseUrl("https://www.rijksmuseum.nl/api/")
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
}