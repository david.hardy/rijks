package nl.codecentric.rijks.backend

import nl.codecentric.rijks.RijksArtDetailCall
import nl.codecentric.rijks.RijksCollectionCall
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface RijksService {
    @GET("nl/collection")
    fun listCollection(@Query("p") page: Int, @Query("ps") pageSize: Int): Call<RijksCollectionCall>

    @GET("nl/collection/{objectNumber}")
    fun artDetails(@Path("objectNumber") objectNumber: String): Call<RijksArtDetailCall>
}