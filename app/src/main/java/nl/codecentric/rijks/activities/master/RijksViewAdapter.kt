package nl.codecentric.rijks.activities.master

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.gson.Gson
import nl.codecentric.rijks.activities.detail.ArtDetailFragment
import nl.codecentric.rijks.ArtDetails
import nl.codecentric.rijks.activities.detail.ItemDetailActivity
import nl.codecentric.rijks.R

class RijksViewAdapter(
    private val fragmentManager: FragmentManager,
    private val twoPane: Boolean,
    private val pageSize: Int,
    private val loadMore: ((currentSize: Int, pageSize: Int) -> Unit)
) :
    RecyclerView.Adapter<RijksViewAdapter.BaseViewHolder>() {
    companion object {
        val VIEWTYPE_LOADING = 0
        val VIEWTYPE_DATA = 1
    }

    private val onClickListener: View.OnClickListener
    private var data: List<ArtDetails?> = listOf(null)
    var errorMessage: String? = null

    init {
        onClickListener = View.OnClickListener { view ->
            val item = view.tag as ArtDetails
            if (twoPane) {
                val fragment = ArtDetailFragment()
                    .apply {
                        arguments = Bundle().apply {
                            putString(ArtDetailFragment.ART_DETAILS_KEY, Gson().toJson(item))
                        }
                    }
                fragmentManager
                    .beginTransaction()
                    .replace(R.id.item_detail_container, fragment)
                    .commit()
            } else {
                val intent = Intent(view.context, ItemDetailActivity::class.java).apply {
                    putExtra(ArtDetailFragment.ART_DETAILS_KEY, Gson().toJson(item))
                }
                view.context.startActivity(intent)
            }
        }
    }

    override fun getItemCount() = data.size

    fun addData(artObjects: List<ArtDetails>) {
        val pos = data.size - 1
        val subList = data.subList(0, data.size - 1)

        data = subList.plus(artObjects).plusElement(null)
        if (pos == 0) {
            notifyDataSetChanged()
        } else {
            notifyItemRangeInserted(pos, artObjects.size)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == data.size - 1) VIEWTYPE_LOADING else VIEWTYPE_DATA
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return if (viewType == VIEWTYPE_DATA) {
            DetailsViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_list_content, parent, false)
            )
        } else {
            LoadingViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_list_loading, parent, false)
            )
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        if (position + pageSize / 2 > data.size) {
            loadMore(data.size - 1, pageSize)
        }

        val details = data[position]
        holder.bind(details)
    }

    fun informError(errorMessage: String) {
        this.errorMessage = errorMessage
        notifyItemChanged(data.size - 1)
    }

    abstract class BaseViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        abstract fun bind(details: ArtDetails?)
    }

    inner class LoadingViewHolder(view: View) : BaseViewHolder(view) {
        private val message: TextView = view.findViewById(R.id.message)

        override fun bind(details: ArtDetails?) {
            message.text = errorMessage
        }
    }

    inner class DetailsViewHolder(view: View) : BaseViewHolder(view) {
        private val nameView: TextView = view.findViewById(R.id.name)
        private val contentView: TextView = view.findViewById(R.id.content)
        private val imageView: ImageView = view.findViewById(R.id.imageView)

        override fun bind(details: ArtDetails?) {
            nameView.text = details?.title
            contentView.text = details?.longTitle

            Glide
                .with(contentView)
                .load(details?.headerImage?.url)
                .centerCrop()
                .placeholder(R.drawable.ic_launcher_background)
                .into(imageView)

            with(itemView) {
                tag = details
                setOnClickListener(onClickListener)
            }
        }
    }
}