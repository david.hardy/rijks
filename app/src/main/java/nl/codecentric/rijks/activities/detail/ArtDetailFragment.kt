package nl.codecentric.rijks.activities.detail

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.google.gson.Gson
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import nl.codecentric.rijks.ArtDetails
import nl.codecentric.rijks.R
import nl.codecentric.rijks.backend.RijksRertofit
import retrofit2.await
import java.lang.Exception

class ArtDetailFragment : Fragment(), CoroutineScope by MainScope() {

    companion object {
        const val ART_DETAILS_KEY = "ART_DETAILS_KEY"
    }

    private var details: ArtDetails? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            if (it.containsKey(ART_DETAILS_KEY)) {
                details = Gson().fromJson(it.getString(ART_DETAILS_KEY)!!, ArtDetails::class.java)
                activity?.findViewById<CollapsingToolbarLayout>(R.id.toolbar_layout)?.title =
                    details?.title

                val toolbarImageView: ImageView? = activity?.findViewById(R.id.toolbarImageView)
                toolbarImageView?.let {
                Glide
                    .with(this)
                    .load(details!!.headerImage.url)
                    .centerCrop()
                    .placeholder(R.drawable.ic_launcher_background)
                    .into(toolbarImageView)
                }
            }
        }
    }

    private val rijksService by lazy { RijksRertofit().rijksService() }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.item_detail, container, false)

        details?.let {
            rootView.findViewById<TextView>(R.id.item_detail_1).text = it.longTitle
            launch {
                try {
                    val await = rijksService.artDetails(it.objectNumber).await()

                    val extendedDetails = await.artObject
                    rootView.findViewById<TextView>(R.id.item_detail_2).text =
                        extendedDetails.label.makerLine
                    rootView.findViewById<TextView>(R.id.item_detail_3).text =
                        extendedDetails.label.description
                    rootView.findViewById<TextView>(R.id.item_detail_4).text =
                        extendedDetails.description
                } catch (e: Exception) {
                    Log.e("TAG", "onCreateView: ", e)
                    rootView.findViewById<TextView>(R.id.item_detail_2).text = "There was an issue obtaining data..."
                    rootView.findViewById<TextView>(R.id.item_detail_3).text = e.message
                }
            }
        }
        return rootView
    }

    override fun onDestroy() {
        super.onDestroy()
        cancel()
    }
}
