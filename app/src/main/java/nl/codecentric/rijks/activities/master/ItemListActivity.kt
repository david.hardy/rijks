package nl.codecentric.rijks.activities.master

import android.os.Bundle
import android.util.Log
import androidx.core.widget.NestedScrollView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import androidx.appcompat.widget.Toolbar
import kotlinx.coroutines.*
import nl.codecentric.rijks.R
import nl.codecentric.rijks.backend.RijksRertofit
import nl.codecentric.rijks.showToast

import retrofit2.await
import java.lang.Exception

class ItemListActivity : AppCompatActivity(), CoroutineScope by MainScope() {

    private var twoPane: Boolean = false
    private var isLoading = false
    private lateinit var adapter: RijksViewAdapter

    private val itemListProvider by lazy { ItemListProvider() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_item_list)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        toolbar.title = title

        twoPane = findViewById<NestedScrollView>(R.id.item_detail_container) != null

        adapter = RijksViewAdapter(
            supportFragmentManager,
            twoPane,
            10
        ) { currentSize: Int, pageSize: Int -> loadMore(currentSize, pageSize) }

        findViewById<RecyclerView>(R.id.item_list).adapter = adapter
    }


    private fun loadMore(currentSize: Int, pageSize: Int) {
        if (isLoading) return
        isLoading = true

        launch {
            try {
                val artDetailList =
                    itemListProvider.getArtDetailList(currentSize / pageSize, pageSize)
                adapter.addData(artDetailList)
                adapter.errorMessage = null
                isLoading = false
            } catch (e: Exception) {
                Log.e("TAG", "loadMore: ", e)
                val errorMessage =
                    "There was an error getting data, please wait... (${e.message}"
                adapter.informError(errorMessage)
                showToast(errorMessage)
                delay(5000)
                isLoading = false
                loadMore(currentSize, pageSize)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        cancel()
    }
}
