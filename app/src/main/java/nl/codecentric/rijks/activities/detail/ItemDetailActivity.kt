package nl.codecentric.rijks.activities.detail

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import nl.codecentric.rijks.R

class ItemDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_item_detail)
        setSupportActionBar(findViewById(R.id.detail_toolbar))

        if (savedInstanceState == null) {
            val fragment = ArtDetailFragment()
                .apply {
                arguments = Bundle().apply {
                    putString(
                        ArtDetailFragment.ART_DETAILS_KEY,
                            intent.getStringExtra(ArtDetailFragment.ART_DETAILS_KEY))
                }
            }

            supportFragmentManager.beginTransaction()
                    .add(R.id.item_detail_container, fragment)
                    .commit()
        }
    }
}