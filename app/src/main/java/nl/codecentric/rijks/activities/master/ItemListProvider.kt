package nl.codecentric.rijks.activities.master

import android.os.Bundle
import android.util.Log
import androidx.core.widget.NestedScrollView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import androidx.appcompat.widget.Toolbar
import kotlinx.coroutines.*
import nl.codecentric.rijks.ArtDetails
import nl.codecentric.rijks.R
import nl.codecentric.rijks.backend.RijksRertofit
import nl.codecentric.rijks.backend.RijksService
import nl.codecentric.rijks.showToast

import retrofit2.await
import java.lang.Exception

class ItemListProvider(private val rijksService: RijksService = RijksRertofit().rijksService()) {
    suspend fun getArtDetailList(page: Int, pageSize: Int): List<ArtDetails> {
        val response = rijksService.listCollection(1 + page, pageSize).await()
        return response.artObjects
    }
}
