package nl.codecentric.rijks

data class ArtDetails (
    val objectNumber : String,
    val title : String,
    val longTitle : String,
    val headerImage : HeaderImage
)

data class ArtExtendedDetails(
    val description: String,
    val label: Label
)

data class HeaderImage (

    val guid : String,
    val offsetPercentageX : Int,
    val offsetPercentageY : Int,
    val width : Int,
    val height : Int,
    val url : String
)

data class Label (

    val title : String,
    val makerLine : String,
    val description : String,
    val notes : String,
    val date : String
)

data class RijksArtDetailCall (
    val artObject : ArtExtendedDetails
)

data class RijksCollectionCall (
    val artObjects : List<ArtDetails>
)